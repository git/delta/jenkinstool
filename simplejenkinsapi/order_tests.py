# simplejenkinsapi/order_tests.py -- unit tests for build ordering
#
# Copyright 2012  Lars Wirzenius
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import unittest

from simplejenkinsapi import order


def _names(projects):
    return [p['name'] for p in projects]


class OrderTests(unittest.TestCase):

    def setUp(self):
        self.A = { 'name': 'A', 'build-depends': [] }
        self.B = { 'name': 'B', 'build-depends': [] }
        self.C = { 'name': 'C', 'build-depends': [] }
        self.D = { 'name': 'D', 'build-depends': [] }

    def test_returns_empty_list_for_empty_list(self):
        self.assertEqual(order([]), [])

    def test_returns_identical_list_for_one_project(self):
        ps = [self.A]
        self.assertEqual(order(ps), ps)

    def test_returns_correct_list_for_two_independent_projects(self):
        ps = [self.A, self.B]
        self.assertEqual(order(ps), ps)

    def test_returns_correct_list_for_two_dependent_projects(self):
        self.A['build-depends'] = ['B']
        ps = [self.A, self.B]
        new = order(ps)
        self.assertEqual(_names(new), _names([self.B, self.A]))

    def test_returns_correct_list_for_three_with_linear_deps(self):
        self.B['build-depends'] = ['A']
        self.C['build-depends'] = ['A']
        ps = [self.A, self.B, self.C]
        self.assertEqual(_names(order(ps)), _names(ps))

    def test_returns_correct_list_for_three_with_nonlinear_deps(self):
        self.A['build-depends'] = ['B', 'C']
        ps = [self.A, self.B, self.C]
        self.assertEqual(_names(order(ps)), _names([self.B, self.C, self.A]))

    def test_returns_correct_list_for_diamond(self):
        self.A['build-depends'] = ['B', 'C']
        self.B['build-depends'] = ['D']
        self.C['build-depends'] = ['D']
        ps = [self.A, self.B, self.C, self.D]
        self.assertEqual(_names(order(ps)),
                         _names([self.D, self.B, self.C, self.A]))

